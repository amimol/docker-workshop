<?php
$host = 'db';
$user = 'root';
$password = 'root';

try {
    $db = new PDO(sprintf('mysql:host=%s;', $host), $user, $password);
    $query = $db->query('SELECT @@version');

    echo $query->fetchColumn();
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
}
