DOCKER WORKSHOP
===============

# 1.Installation
To install docker & docker-compose execute
```
curl -fsSL https://get.docker.com/ | sh
sudo sh -c "curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
sudo usermod -a -G docker $USER
sudo service docker start
```
and reboot your machine
```
sudo reboot now
```

#2. Usuage
To start docker go do `.docker` directory end execute `docker-compose up -d` to fetch and create containers.
Next time `docker-compose start` will be enough.

#3. Usefull commands
`docker exec -it [container_name] bash` - to enter to container

`docker ps` - to list running containers

`docker images -a` - to list all images

`docker-compose logs` - to get logs form containers started by docker compose

`docler log [container_name]` - to get log form contaienr

`docker run [image] [app]` - to run container based on image and execute app

